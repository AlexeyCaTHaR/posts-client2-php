# Ensi\BackendServiceSkeletonClient\RatingApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVote**](RatingApi.md#createVote) | **POST** /votes | Создание объекта типа Vote
[**deleteVote**](RatingApi.md#deleteVote) | **DELETE** /votes/{id} | Удаление объекта типа Post
[**replaceVote**](RatingApi.md#replaceVote) | **PUT** /votes/{id} | Замена объекта типа Vote
[**searchVote**](RatingApi.md#searchVote) | **POST** /votes:search | Поиск объектов типа Vote



## createVote

> \Ensi\BackendServiceSkeletonClient\Dto\VoteResponse createVote()

Создание объекта типа Vote

Создание объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\RatingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->createVote();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatingApi->createVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteVote

> \Ensi\BackendServiceSkeletonClient\Dto\EmptyDataResponse deleteVote($id, $include)

Удаление объекта типа Post

Удаление объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\RatingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->deleteVote($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatingApi->deleteVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceVote

> \Ensi\BackendServiceSkeletonClient\Dto\VoteResponse replaceVote($id, $replace_vote_request)

Замена объекта типа Vote

Замена объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\RatingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_vote_request = new \Ensi\BackendServiceSkeletonClient\Dto\ReplaceVoteRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\ReplaceVoteRequest | 

try {
    $result = $apiInstance->replaceVote($id, $replace_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatingApi->replaceVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_vote_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\ReplaceVoteRequest**](../Model/ReplaceVoteRequest.md)|  |

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchVote

> \Ensi\BackendServiceSkeletonClient\Dto\SearchVoteResponse searchVote($search_vote_request)

Поиск объектов типа Vote

Поиск объектов типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\RatingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_vote_request = new \Ensi\BackendServiceSkeletonClient\Dto\SearchVoteRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\SearchVoteRequest | 

try {
    $result = $apiInstance->searchVote($search_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RatingApi->searchVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_vote_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\SearchVoteRequest**](../Model/SearchVoteRequest.md)|  |

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\SearchVoteResponse**](../Model/SearchVoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

