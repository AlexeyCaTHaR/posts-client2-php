# Ensi\BackendServiceSkeletonClient\PostApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPost**](PostApi.md#createPost) | **POST** /posts | Создание объекта типа Post
[**deletePost**](PostApi.md#deletePost) | **DELETE** /posts/{id} | Удаление объекта типа Post
[**getPost**](PostApi.md#getPost) | **GET** /posts/{id} | Получение объекта типа Post
[**patchPost**](PostApi.md#patchPost) | **PATCH** /posts/{id} | Обновления части полей объекта типа Post
[**replacePost**](PostApi.md#replacePost) | **PUT** /posts/{id} | Замена объекта типа Post
[**searchOnePost**](PostApi.md#searchOnePost) | **POST** /posts:search-one | Поиск объекта типа Post
[**searchPost**](PostApi.md#searchPost) | **POST** /posts:search | Поиск объектов типа Post



## createPost

> \Ensi\BackendServiceSkeletonClient\Dto\PostResponse createPost()

Создание объекта типа Post

Создание объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->createPost();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->createPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePost

> \Ensi\BackendServiceSkeletonClient\Dto\EmptyDataResponse deletePost($id, $include)

Удаление объекта типа Post

Удаление объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->deletePost($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->deletePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPost

> \Ensi\BackendServiceSkeletonClient\Dto\PostResponse getPost($id, $include)

Получение объекта типа Post

Получение объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getPost($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->getPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPost

> \Ensi\BackendServiceSkeletonClient\Dto\PostResponse patchPost($id, $patch_post_request)

Обновления части полей объекта типа Post

Обновления части полей объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_post_request = new \Ensi\BackendServiceSkeletonClient\Dto\PatchPostRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\PatchPostRequest | 

try {
    $result = $apiInstance->patchPost($id, $patch_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->patchPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_post_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\PatchPostRequest**](../Model/PatchPostRequest.md)|  |

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replacePost

> \Ensi\BackendServiceSkeletonClient\Dto\PostResponse replacePost($id, $replace_post_request)

Замена объекта типа Post

Замена объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_post_request = new \Ensi\BackendServiceSkeletonClient\Dto\ReplacePostRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\ReplacePostRequest | 

try {
    $result = $apiInstance->replacePost($id, $replace_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->replacePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_post_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\ReplacePostRequest**](../Model/ReplacePostRequest.md)|  |

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOnePost

> OneOfSearchPostResponseEmptyDataResponse searchOnePost($search_post_request)

Поиск объекта типа Post

Поиск объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_post_request = new \Ensi\BackendServiceSkeletonClient\Dto\SearchPostRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\SearchPostRequest | 

try {
    $result = $apiInstance->searchOnePost($search_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->searchOnePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_post_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\SearchPostRequest**](../Model/SearchPostRequest.md)|  |

### Return type

[**OneOfSearchPostResponseEmptyDataResponse**](../Model/OneOfSearchPostResponseEmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPost

> \Ensi\BackendServiceSkeletonClient\Dto\SearchPostResponse searchPost($search_post_request)

Поиск объектов типа Post

Поиск объектов типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BackendServiceSkeletonClient\Api\PostApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_post_request = new \Ensi\BackendServiceSkeletonClient\Dto\SearchPostRequest(); // \Ensi\BackendServiceSkeletonClient\Dto\SearchPostRequest | 

try {
    $result = $apiInstance->searchPost($search_post_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostApi->searchPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_post_request** | [**\Ensi\BackendServiceSkeletonClient\Dto\SearchPostRequest**](../Model/SearchPostRequest.md)|  |

### Return type

[**\Ensi\BackendServiceSkeletonClient\Dto\SearchPostResponse**](../Model/SearchPostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

