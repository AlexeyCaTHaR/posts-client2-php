# # Post

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поста | [optional] 
**rating** | **int** | Рейтинг поста | [optional] 
**title** | **string** | Название поста | [optional] 
**description** | **string** | Название поста | [optional] 
**content** | **string** | Название поста | [optional] 
**user_id** | **int** | Идентификатор пользователя | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


