# # SearchVoteRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**filter** | [**\Ensi\BackendServiceSkeletonClient\Dto\SearchVoteFilter**](SearchVoteFilter.md) |  | [optional] 
**pagination** | [**\Ensi\BackendServiceSkeletonClient\Dto\RequestBodyPagination**](RequestBodyPagination.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


