# # ReplaceVoteRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **int** | Идентификатор пользователя | [optional] 
**post_id** | **int** | Идентификатор поста | [optional] 
**vote** | **int** | Дельта голоса | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


