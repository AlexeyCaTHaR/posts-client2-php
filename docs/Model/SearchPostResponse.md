# # SearchPostResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\BackendServiceSkeletonClient\Dto\Post[]**](Post.md) |  | 
**meta** | [**\Ensi\BackendServiceSkeletonClient\Dto\SearchPostResponseMeta**](SearchPostResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


