# # SearchVoteResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\BackendServiceSkeletonClient\Dto\Vote[]**](Vote.md) |  | 
**meta** | [**\Ensi\BackendServiceSkeletonClient\Dto\SearchPostResponseMeta**](SearchPostResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


