<?php
/**
 * PaginationTypeEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\BackendServiceSkeletonClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Second practise task
 *
 * Second practise task
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: a.filippov@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\BackendServiceSkeletonClient\Dto;
use \Ensi\BackendServiceSkeletonClient\ObjectSerializer;

/**
 * PaginationTypeEnum Class Doc Comment
 *
 * @category Class
 * @description Pagination types: * &#x60;cursor&#x60; - Пагинация используя cursor * &#x60;offset&#x60; - Пагинация используя offset
 * @package  Ensi\BackendServiceSkeletonClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class PaginationTypeEnum
{
    /**
     * Possible values of this enum
     */
    const CURSOR = 'cursor';
    const OFFSET = 'offset';
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::CURSOR,
            self::OFFSET,
        ];
    }
}


