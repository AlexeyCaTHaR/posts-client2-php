<?php

namespace Ensi\BackendServiceSkeletonClient;

class BackendServiceSkeletonClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\BackendServiceSkeletonClient\Api\RatingApi',
        '\Ensi\BackendServiceSkeletonClient\Api\PostApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\BackendServiceSkeletonClient\Dto\VoteFillableProperties',
        '\Ensi\BackendServiceSkeletonClient\Dto\PostFillableProperties',
        '\Ensi\BackendServiceSkeletonClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchVoteFilter',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchVoteResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\PaginationTypeEnum',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchPostRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\PostResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchPostResponseMeta',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchPostFilter',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchVoteRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\ReplacePostRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\ErrorResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\PostReadonlyProperties',
        '\Ensi\BackendServiceSkeletonClient\Dto\PatchPostRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\ResponseBodyPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\ModelInterface',
        '\Ensi\BackendServiceSkeletonClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\BackendServiceSkeletonClient\Dto\EmptyDataResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\RequestBodyPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\Post',
        '\Ensi\BackendServiceSkeletonClient\Dto\ReplaceVoteRequest',
        '\Ensi\BackendServiceSkeletonClient\Dto\Vote',
        '\Ensi\BackendServiceSkeletonClient\Dto\SearchPostResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\Error',
        '\Ensi\BackendServiceSkeletonClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\BackendServiceSkeletonClient\Dto\RequestBodyCursorPagination',
        '\Ensi\BackendServiceSkeletonClient\Dto\VoteResponse',
        '\Ensi\BackendServiceSkeletonClient\Dto\ResponseBodyCursorPagination',
    ];

    /** @var string */
    public static $configuration = '\Ensi\BackendServiceSkeletonClient\Configuration';
}
